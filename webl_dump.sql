-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: webbylab
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.17.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `film_to_star`
--

DROP TABLE IF EXISTS `film_to_star`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `film_to_star` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `film_id` int(11) unsigned NOT NULL,
  `star_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `film_to_star_films_id_fk` (`film_id`),
  KEY `film_to_star_stars_id_fk` (`star_id`),
  CONSTRAINT `film_to_star_films_id_fk` FOREIGN KEY (`film_id`) REFERENCES `films` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `film_to_star_stars_id_fk` FOREIGN KEY (`star_id`) REFERENCES `stars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `film_to_star`
--

LOCK TABLES `film_to_star` WRITE;
/*!40000 ALTER TABLE `film_to_star` DISABLE KEYS */;
INSERT INTO `film_to_star` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,2,7),(8,2,8),(9,2,9),(10,2,10),(11,3,11),(12,3,12),(13,3,13),(14,3,14),(15,3,15),(16,1,10),(23,1,1),(24,1,2),(25,1,3),(26,1,4),(27,1,5),(28,1,6),(29,38,32),(30,38,33),(31,38,34),(39,1,1),(40,1,2),(41,1,3),(42,1,4),(43,1,5),(44,1,6),(45,38,32),(46,38,33),(47,38,34),(66,52,69),(67,52,70),(68,52,71),(69,52,72),(70,1,1),(71,1,2),(72,1,3),(73,1,4),(74,1,5),(75,1,6),(76,38,32),(77,38,33),(78,38,34),(79,52,69),(80,52,70),(81,52,71),(82,52,72),(83,1,1),(84,1,2),(85,1,3),(86,1,4),(87,1,5),(88,1,6),(89,38,32),(90,38,33),(91,38,34),(92,52,69),(93,52,70),(94,52,71),(95,52,72),(96,1,1),(97,1,2),(98,1,3),(99,1,4),(100,1,5),(101,1,6),(102,38,32),(103,38,33),(104,38,34),(105,52,69),(106,52,70),(107,52,71),(108,52,72),(109,1,1),(110,1,2),(111,1,3),(112,1,4),(113,1,5),(114,1,6),(115,38,32),(116,38,33),(117,38,34);
/*!40000 ALTER TABLE `film_to_star` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `films`
--

DROP TABLE IF EXISTS `films`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `films` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `release_year` smallint(4) unsigned NOT NULL,
  `format` set('VHS','DVD','Blu-Ray') NOT NULL,
  `updated_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_release_year_unique_index` (`title`,`release_year`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `films`
--

LOCK TABLES `films` WRITE;
/*!40000 ALTER TABLE `films` DISABLE KEYS */;
INSERT INTO `films` VALUES (1,'Blazing Saddles',1974,'VHS','2018-09-16 23:03:37'),(2,'Casablanca',1942,'DVD',NULL),(3,'Charade',1953,'DVD',NULL),(38,'Hooisers',1986,'VHS','2018-09-16 23:03:37'),(52,'Knocked Up',2007,'VHS','2018-09-16 23:03:37');
/*!40000 ALTER TABLE `films` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stars`
--

DROP TABLE IF EXISTS `stars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stars` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `updated_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stars_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stars`
--

LOCK TABLES `stars` WRITE;
/*!40000 ALTER TABLE `stars` DISABLE KEYS */;
INSERT INTO `stars` VALUES (1,'Mel Brooks','2018-09-16 23:03:37'),(2,'Clevon Little','2018-09-16 23:03:37'),(3,'Harvey Korman','2018-09-16 23:03:37'),(4,'Gene Wilder','2018-09-16 23:03:37'),(5,'Slim Pickens','2018-09-16 23:03:37'),(6,'Madeline Kahn','2018-09-16 23:03:37'),(7,'Humphrey Bogart',NULL),(8,'Ingrid Bergman',NULL),(9,'Claude Rains',NULL),(10,'Peter Lorre',NULL),(11,'Audrey Hepburn',NULL),(12,'Cary Grant',NULL),(13,'Walter Matthau',NULL),(14,'James Coburn',NULL),(15,'George Kennedy',NULL),(32,'Gene Hackman','2018-09-16 23:03:37'),(33,'Barbara Hershey','2018-09-16 23:03:37'),(34,'Dennis Hopper','2018-09-16 23:03:37'),(69,'Seth Rogen','2018-09-16 23:03:37'),(70,'Katherine Heigl','2018-09-16 23:03:37'),(71,'Paul Rudd','2018-09-16 23:03:37'),(72,'Leslie Mann','2018-09-16 23:03:37');
/*!40000 ALTER TABLE `stars` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-16 23:24:44
