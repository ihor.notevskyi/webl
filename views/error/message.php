<div class="text-center error-wrapper">
    <img src="/img/error.png" alt="Error">
    <div>
        <p>Error!</p>
        <p>The requested page can not be displayed.</p>
        <p>Please try again later.</p>
    </div>
</div>