<div class="text-left container upload">
    <h1 class="text-center">Upload text file</h1>
    <form method="post" enctype="multipart/form-data" id="form_upload">
        <div class="custom-file was-validated">
            <input type="file" name="text-file" class="custom-file-input" id="upload_file" required>
            <label class="custom-file-label" for="upload_file">Choose your file...</label>
            <div class="invalid-feedback">File is not uploaded</div>
        </div>
        <input type="submit" name="submit-file" class="btn btn-lg btn-success" value="Upload file">
    </form>
    <?php $i = 1;
    if ($data['errors']) : ?>
        <h2 class="text-center font-weight-bold">Errors</h2>
        <?php foreach ($data['errors'] as $error) : ?>
            <hr>
            <p class="text-danger"><span class="text-dark"><?= $i . '. ' ?></span><?php echo $error; $i++ ?></p>
            <hr>
        <?php endforeach;
    endif; ?>
</div>