<h1 class="text-center films">Films list</h1>
<br>
<div class="d-flex justify-content-between">
    <div class="btn-group dropright">
        <button type="button" class="btn btn-lg btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span>Sort by films title</span>
        </button>
        <div class="dropdown-menu">
            <a href="/films?orderBy=title&sort=asc" class="dropdown-item">in alphabetical order</a>
            <a href="/films?orderBy=title&sort=desc" class="dropdown-item">in reverse alphabetical order</a>
        </div>
    </div>
    <form action="/films" enctype="application/x-www-form-urlencoded" class="form-inline">
        <input type="text" placeholder="Search by stars" name="star" class="form-control">
        <button type="submit" class="btn btn-default search-films">Search</button>
    </form>
    <a href="/films/upload" class="upload-button">
        <button class="btn btn-lg btn-info">Upload file</button>
    </a>
    <a href="/films/add" class="add-button">
        <button class="btn btn-lg btn-success">Add new film</button>
    </a>
</div>
<br>
<div class="table-responsive text-center table-films">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Release Year</th>
                <th>Format</th>
                <th>Stars</th>
                <th>Control</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data['films'] as $key => $film) : ?>
                <?php if ($key === 'count') break; ?>
                <tr>
                    <td><?= $film['id'] ?></td>
                    <td><?= $film['title'] ?></td>
                    <td><?= $film['release_year'] ?></td>
                    <td><?= str_replace(',', ', ', $film['format']) ?></td>
                    <td><?= $film['stars_name'] ?></td>
                    <td>
                        <button class="btn btn-sm btn-block btn-danger delete-items" id="del_<?= $film['id'] ?>">Delete</button>
                    </td>
                </tr>
            <?php endforeach; ?>
        <tbody>
    </table>
</div>
<br>
<?php if (!isset($_GET['page'])) $_GET['page'] = 1; ?>
<div class="text-center pagination-wrapper">
    <ul class="pagination pagination-lg films-pagination justify-content-center">
        <?php if ($_GET['page'] > 1) : ?>
            <li class="page-item">
                <a class="page-link" href="/films?page=<?= $_GET['page'] - 1 ?><?= $_GET['orderBy'] && $_GET['sort'] ? '&orderBy=' . $_GET['orderBy'] . '&sort=' . $_GET['sort'] : '' ?><?= $_GET['star'] ? '&star=' . $_GET['star'] : '' ?>">Previous</a>
            </li>
        <?php else : ?>
            <li class="page-item disabled">
                <span class="page-link">Previous</span>
            </li>
        <?php endif; ?>

        <?php for ($j = 1; $j <= $data['films']['count']; $j++) : ?>
            <?php if ($j == 2) : ?>
                <li class="page-item dots" onclick="moveDots()">
                    <span class="page-link">...</span>
                </li>
            <?php endif; ?>
            <li class="page-item<?= ($j == $_GET['page']) ? ' active' : ''; ?>">
                <a href="/films?page=<?= $j ?><?= $_GET['orderBy'] && $_GET['sort'] ? '&orderBy=' . $_GET['orderBy'] . '&sort=' . $_GET['sort'] : '' ?><?= $_GET['star'] ? '&star=' . $_GET['star'] : '' ?>" class="page-link"><?= $j ?></a>
            </li>
        <?php endfor; ?>

        <?php if ($_GET['page'] < $j - 1) : ?>
            <li class="page-item">
                <a class="page-link" href="/films?page=<?= $_GET['page'] + 1 ?><?= $_GET['orderBy'] && $_GET['sort'] ? '&orderBy=' . $_GET['orderBy'] . '&sort=' . $_GET['sort'] : '' ?><?= $_GET['star'] ? '&star=' . $_GET['star'] : '' ?>">Next</a>
            </li>
        <?php else : ?>
            <li class="page-item disabled">
                <span class="page-link">Next</span>
            </li>
        <?php endif; ?>
    </ul>
</div>
<script src="/js/pagination.js"></script>
<script src="/js/delete.js"></script>