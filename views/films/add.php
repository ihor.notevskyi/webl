<div class="text-left container add-film">
    <h1 class="text-center">Add new film</h1>
    <form action="" method="post" id="form_add">
        <div class="form-group">
            <label for="add_title">Title</label>
            <input type="text" id="add_title" name="title" class="form-control" placeholder="Enter film name">
            <small class="text-danger">
                <?php if (array_key_exists('title', $data['errors'])) : ?>
                    <span><?= $data['errors']['title'] ?></span>
                <?php endif; ?>
            </small>
        </div>
        <div class="form-group add-release-year">
            <label for="add_release-year">Release year</label>
            <input type="text" id="add_release-year" name="release-year" class="form-control" placeholder="Example: 2018">
            <small class="text-danger">
                <?php if (array_key_exists('release-year', $data['errors'])) : ?>
                    <span><?= $data['errors']['release-year'] ?></span>
                <?php endif; ?>
            </small>
        </div>
        <div class="form-group">
            <label for="add_release-format">Format</label>
            <select class="form-control" name="format" id="add_release-format">
                <option value="" selected disabled>Select format</option>
                <option value="VHS">VHS</option>
                <option value="DVD">DVD</option>
                <option value="Blu-Ray">Blu-Ray</option>
            </select>
            <small class="text-danger">
                <?php if (array_key_exists('format', $data['errors'])) : ?>
                    <span><?= $data['errors']['format'] ?></span>
                <?php endif; ?>
            </small>
        </div>
        <div class="form-group">
            <label for="add_format">Stars</label>
            <textarea id="add_format" name="stars" rows="4" class="form-control" placeholder="List stars through a comma. Example: John Wood, Mike Dou, Ellis Carpenter"></textarea>
            <small class="text-danger">
                <?php if (array_key_exists('stars', $data['errors'])) : ?>
                    <span><?= $data['errors']['stars'] ?></span>
                <?php endif; ?>
            </small>
        </div>
        <input type="submit" class="btn btn-lg btn-success" value="Save film">
    </form>
</div>