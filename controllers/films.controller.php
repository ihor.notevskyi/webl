<?php

class FilmsController extends Controller
{
    const FORMAT_VHS = 'VHS';
    const FORMAT_DVD = 'DVD';
    const FORMAT_BLU_RAY = 'Blu-Ray';

    private $formatValues = [
        self::FORMAT_VHS,
        self::FORMAT_DVD,
        self::FORMAT_BLU_RAY
    ];

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        $this->model = new Film();
    }

    public function index()
    {
        if (isset($_GET['page'])) {
            $page = (int)strip_tags($_GET['page'] - 1);
        }
        $page = !isset($page) ? 0 : $page;

        $orderByValues = 'title';
        $sortValues = ['asc', 'desc'];
        $orderBy = '';
        $sort = '';
        $filter = '';

        if (isset($_GET['orderBy']) && isset($_GET['sort']) && ($_GET['orderBy'] === $orderByValues) && in_array($_GET['sort'], $sortValues)) {
            $orderBy = strip_tags($_GET['orderBy']);
            $sort = strtoupper(strip_tags($_GET['sort']));
        }

        if (isset($_GET['star'])) {
            $filter = trim(strip_tags($_GET['star']));
        }

        $this->data['films'] = $this->model->getFilmsListByPage($page, $orderBy, $sort, $filter);
    }

    public function add()
    {
        if ($_POST) {
            $this->data['errors'] = $this->checkFilmsFields($_POST);

            if (empty($this->data['errors'])) {
                $dataFilm['Title'] = $this->filterString($_POST['title']);
                $dataFilm['Release Year'] = $this->filterString($_POST['release-year']);
                $dataFilm['Format'] = $this->filterString($_POST['format']);
                $dataFilm['Stars'] = $this->filterString($_POST['stars']);

                try {
                    $this->model->addFilm([[$dataFilm]]);
                    Session::setFlash('Film successfully added');
                } catch (Exception $e) {
                    throw $e;
                }
            } else {
                Session::setFlash('Film was not added');
            }
        }
    }

    public function delete()
    {
        if (isset($this->params[0])) {
            $paramsId = (int)strip_tags($this->params[0]);
            $this->model->deleteFilm($paramsId);

            Router::redirect('/');
        }
    }

    public function upload()
    {
        if ($_POST && $_POST['submit-file'] && $_FILES['text-file'] && $_FILES['text-file']['type'] === 'text/plain' && $_FILES['text-file']['size'] > 0) {
            $handle = fopen($_FILES['text-file']['tmp_name'], 'r');
            $films = preg_split('/\r?\n\r?\n/', fread($handle, filesize($_FILES['text-file']['tmp_name'])));
            fclose($handle);

            $errors = [];
            foreach ($films as $film) {
                if (trim($film, " \t\n\r\0\x0B") == false) continue;
                $filmsValues = preg_split('/\r?\n/', $film);

                $associativeFilmsValues = [];
                foreach ($filmsValues as $filmsValue) {
                    $indexedFilmsValues = explode(':', $filmsValue);
                    $associativeFilmsValues[$this->filterString($indexedFilmsValues[0])] = $this->filterString($indexedFilmsValues[1]);
                }

                $filmWithError = "in film ('Title': {$associativeFilmsValues['Title']}; 'Release Year': {$associativeFilmsValues['Release Year']}; 'Format': {$associativeFilmsValues['Format']}; 'Stars': {$associativeFilmsValues['Stars']})";

                if ($associativeFilmsValues['Title'] === '') {
                    $errors[] = "'Title' is required field {$filmWithError}";
                }
                if (mb_strlen($associativeFilmsValues['Title']) > 50) {
                    $errors[] = "Maximum length of field 'Title' is 50 characters {$filmWithError}";
                }
                if ($associativeFilmsValues['Release Year'] === '') {
                    $errors[] = "'Release year' is required field {$filmWithError}";
                }
                if (!preg_match('/^\d\d\d\d$/', $associativeFilmsValues['Release Year'])) {
                    $errors[] = "Entered an incorrect value in field 'Release year' {$filmWithError}";
                }
                if (!in_array($associativeFilmsValues['Format'], $this->formatValues, true)) {
                    $errors[] = "Entered an incorrect value in field 'Format' (required field) {$filmWithError}";
                }
                if ($associativeFilmsValues['Stars'] === '') {
                    $errors[] = "'Stars' is Required field {$filmWithError}";
                } else {
                    $stars = explode(',', $associativeFilmsValues['Stars']);

                    foreach ($stars as $star) {
                        $star = trim($star);
                        if (mb_strlen($star) > 50) {
                            $errors[] = "Incorrect star name '{$star}'. Maximum length of one star name is 50 characters {$filmWithError}";
                        }
                    }
                }

                if (count($errors) > 0) {
                    $this->data['errors'] = $errors;
                    Session::setFlash('File was not uploaded');

                    return false;
                } else {
                    $allFilmsFromFile[] = $associativeFilmsValues;
                }
            }

            $chunksAllFilmsFromFile = array_chunk($allFilmsFromFile, 2); // 2 is the test value of the chunks, in reality this value is much larger (for example 1000 or more)

            try {
                $this->model->addFilm($chunksAllFilmsFromFile);
                Session::setFlash('File is successfully uploaded');
            } catch (Exception $e) {
                throw $e;
            }
        }
    }

    private function filterString($string)
    {
        return preg_replace('/\s+/', ' ', trim(strip_tags($string)));
    }

    private function checkFilmsFields($postData)
    {
        $errors = [];

        if ($postData['title'] === '') {
            $errors['title'] = '"Title" is required field';
        }
        if (mb_strlen($postData['title']) > 50) {
            $errors['title'] = 'Maximum length of field "Title" is 50 characters';
        }
        if ($postData['release-year'] === '') {
            $errors['release-year'] = '"Release year" is required field';
        }
        if (!preg_match('/^\d\d\d\d$/', $postData['release-year'])) {
            $errors['release-year'] = 'You entered an incorrect value in field "Release year"';
        }
        if (!in_array($postData['format'], $this->formatValues, true)) {
            $errors['format'] = '"Format" is required field';
        }
        if ($postData['stars'] === '') {
            $errors['stars'] = '"Stars" is Required field';
        } else {
            $stars = explode(',', $postData['stars']);
            foreach ($stars as $star) {
                if (mb_strlen($star) > 50) {
                    $errors['stars'] = 'Maximum length of one star name is 50 characters';
                }
            }
        }

        return $errors;
    }
}
