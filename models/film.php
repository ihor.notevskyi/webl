<?php

class Film extends Model
{
    const PAGE_SIZE = 2;

    public function getFilmsListByPage($page, $orderBy, $sort, $filter)
    {
        $page = $this->db->escape($page);
        $orderBy = $this->db->escape($orderBy);
        $sort = $this->db->escape($sort);
        $filter = $this->db->escape($filter);

        $limit = self::PAGE_SIZE;
        $start = $page * $limit;

        $sql = "SELECT f.id,
                       f.title,  
                       f.release_year,  
                       f.format,  
                       GROUP_CONCAT(s.name SEPARATOR ', ') AS stars_name 
                FROM `films` f
                    INNER JOIN `film_to_star` fs on f.id = fs.film_id
                    INNER JOIN `stars` s on fs.star_id = s.id 
                    GROUP BY f.id ";

        if ($filter) {
            $sql .= "HAVING GROUP_CONCAT(s.name) LIKE '%{$filter}%' ";
        }

        if ($orderBy && $sort) {
            $sql .= "ORDER BY f.{$orderBy} {$sort}";
        } else {
            $sql .= "ORDER BY f.release_year DESC";
        }

        $result = $this->db->query($sql);

        $numRows = count($result);
        $result = array_slice($result, $start, $limit);
        $result['count'] = ceil($numRows / $limit);

        if ($_GET['page'] > $result['count']) {
            throw new \Exception('Page not found');
        }

        return $result;
    }

    public function insertFilmsFromFile($title, $releaseYear, $format)
    {
        $title = $this->db->escape($title);
        $releaseYear = $this->db->escape($releaseYear);
        $format = $this->db->escape($format);

        $sql = "INSERT INTO `films` (`title`, `release_year`, `format`) 
                    VALUES ('{$title}', '{$releaseYear}', '{$format}')
                    ON DUPLICATE KEY UPDATE `format` = CONCAT_WS(',', `format`, VALUES(`format`)),
                                            `updated_time` = NOW()";

        $this->prepareAndExecute($sql);

        return $this->db->insert_id;
    }

    public function insertStarsFromFile($name)
    {
        $name = $this->db->escape($name);

        $sql = "INSERT INTO `stars` (`name`) 
                    VALUES ('{$name}')
                    ON DUPLICATE KEY UPDATE `updated_time` = NOW()";

        $this->prepareAndExecute($sql);

        return $this->db->insert_id;
    }

    public function addRelations($filmInsertId, $starInsertId)
    {
        $filmInsertId = $this->db->escape($filmInsertId);
        $starInsertId = $this->db->escape($starInsertId);

        $sql = "INSERT IGNORE INTO `film_to_star` (film_id, star_id) 
                    VALUES ({$filmInsertId}, {$starInsertId})";

        $this->prepareAndExecute($sql);
    }

    public function addFilm($data)
    {
        foreach ($data as $chunks) {
            $this->db->autocommit(false);
            $this->db->beginTransaction();

            try {
                foreach ($chunks as $chunk) {
                    $filmInsertId = $this->insertFilmsFromFile($chunk['Title'], $chunk['Release Year'], $chunk['Format']);

                    $chunkStars = array_map('trim', explode(',', $chunk["Stars"]));
                    foreach ($chunkStars as $chunkStar) {
                        $starInsertId = $this->insertStarsFromFile($chunkStar);
                        $this->addRelations($filmInsertId, $starInsertId);
                    }
                }

                $commit = $this->db->commit();
                if (!$commit) {
                    throw new Exception('Not commit transaction');
                }
            } catch (Exception $e) {
                $this->db->rollback();
                throw $e;
            }
        }
    }

    public function deleteFilm($id)
    {
        $id = $this->db->escape($id);

        $sqlFilm = "DELETE FROM `films` 
                        WHERE id = {$id}";

        $this->prepareAndExecute($sqlFilm);

        $sqlStars = "DELETE FROM stars 
                         WHERE stars.id NOT IN (SELECT film_to_star.star_id FROM film_to_star);";

        $this->prepareAndExecute($sqlStars);
    }

    private function prepareAndExecute($sql)
    {
        $stmt = $this->db->prepare($sql);
        if (!$stmt) {
            throw new Exception($this->db->error);
        }

        $result = $stmt->execute();
        if (!$result) {
            throw new Exception($stmt->error);
        }
    }
}
