<?php

class Model
{
    protected $db;

    public function __construct()
    {
        $this->db = App::$db;
    }

    public function __call($method, $args)
    {
        if (is_callable(array($this->db, $method))) {
            return call_user_func_array(array($this->db, $method), $args);
        } else {
            trigger_error("Call to undefined method '{$method}'");
        }
    }
}