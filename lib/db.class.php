<?php

class DB
{
    protected $connection;

    public function __construct($host, $user, $password, $databaseName)
    {
        $this->connection = new mysqli($host, $user, $password, $databaseName);
        $sql = "SET NAMES utf8";
        $this->connection->query($sql);

        if (mysqli_connect_error()) {
            throw new Exception('Could not connect to DB');
        }
    }

    public function __call($method, $args)
    {
        if (is_callable(array($this->connection, $method))) {
            return call_user_func_array(array($this->connection, $method), $args);
        } else {
            trigger_error("Call to undefined method '{$method}'");
        }
    }

    public function __get($name)
    {
        return $this->connection->$name;
    }

    public function query($sql)
    {
        if (!$this->connection) {
            return false;
        }

        $result = $this->connection->query($sql);

        if (mysqli_error($this->connection)) {
            throw new Exception(mysqli_error($this->connection));
        }

        if (is_bool($result)) {
            return $result;
        }

        $data = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $data[] = $row;
        }

        return $data;
    }

    // Prevention of SQL injections
    public function escape($str)
    {
        return mysqli_escape_string($this->connection, $str);
    }
}